import 'package:flutter/material.dart';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 109, 11, 11);
  static Color colorPrimary = Color.fromARGB(255, 109, 11, 11);
  static Color colorSecondary = Color.fromARGB(255, 109, 11, 11);
  static Color colorAccent = Color.fromARGB(255, 109, 11, 11);
}
